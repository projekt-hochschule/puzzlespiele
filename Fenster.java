
/**
 * Klasse ZeichnenFrame.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */

import javax.swing.JFrame;
import javax.swing.JPanel;

public class Fenster extends JFrame
{
    public Fenster()
    {
        JPanel panel = new MyPanel(this);
        add(panel);
        
        setTitle("Game");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        setLocationRelativeTo(null);
        
        pack();
    }
}
