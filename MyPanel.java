
/**
 * Klasse ZeichnenPanel.
 * 
 * @author (Ihr Name) 
 * @version (eine Versionsnummer oder ein Datum)
 */

import java.awt.*;
import java.awt.event.*;

import javax.swing.JPanel;
import javax.swing.JFrame;
import javax.swing.Timer;

public class MyPanel extends JPanel implements ActionListener
{
    private Fenster frame;
    private Timer timer;

    private final int PANEL_HOEHE  = 600;
    private final int PANEL_BREITE = 800;
    private final int DELAY_MS = 50;
    
    private final int LENGTH_SCHLAEGER = 100;

    private int x = PANEL_BREITE/2;
    private int y = PANEL_HOEHE/2;

    private int v = 5;

    private int player1 = PANEL_HOEHE/2;

    public MyPanel(Fenster frame)
    {
        this.frame = frame;
        setPreferredSize(new Dimension(PANEL_BREITE, PANEL_HOEHE));
        setFocusable(true);
        addKeyListener(new myKeyAdapter());

        starteSpiel();
    }

    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);

        g.fillOval(x, y,10,10);
        g.fillRect( PANEL_BREITE - 20, player1- LENGTH_SCHLAEGER/2, 8, LENGTH_SCHLAEGER);
    }   

    private void starteSpiel()
    {
        Timer timer = new Timer(DELAY_MS, this);
        timer.start();
    }

    public void actionPerformed(ActionEvent e)
    {
        x = x + v;
        repaint();
    }

    public class myKeyAdapter extends KeyAdapter
    {
        public void keyPressed(KeyEvent e)
        {
            switch(e.getKeyCode()){
                case KeyEvent.VK_UP:
                player1 -= 10;
                break;
                case KeyEvent.VK_DOWN:
                player1 += 10;
                break;
            }
            repaint();
        }
    }
}
